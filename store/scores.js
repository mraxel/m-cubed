/* SCORE STORE
 * This store is for storing the scores that are used in the calculation for
 *  each speaker. It is read by a few different places, and used to determine
 *  some other factors such
 */

export const state = () => ({
  aff: {
    first: {
      matter: 30,
      method: 15,
      manner: 30,
    },
    second: {
      matter: 30,
      method: 15,
      manner: 30,
    },
    third: {
      matter: 30,
      method: 15,
      manner: 30,
    },
  },
  neg: {
    first: {
      matter: 30,
      method: 15,
      manner: 30,
    },
    second: {
      matter: 30,
      method: 15,
      manner: 30,
    },
    third: {
      matter: 30,
      method: 15,
      manner: 30,
    },
  },
})

export const getters = {
  all: (state) => {
    return {
      aff: {
        first: {
          matter: state.aff.first.matter,
          method: state.aff.first.method,
          manner: state.aff.first.manner,
          total:
            state.aff.first.matter +
            state.aff.first.method +
            state.aff.first.manner,
        },
        second: {
          matter: state.aff.second.matter,
          method: state.aff.second.method,
          manner: state.aff.second.manner,
          total:
            state.aff.second.matter +
            state.aff.second.method +
            state.aff.second.manner,
        },
        third: {
          matter: state.aff.third.matter,
          method: state.aff.third.method,
          manner: state.aff.third.manner,
          total:
            state.aff.third.matter +
            state.aff.third.method +
            state.aff.third.manner,
        },
        total: {
          matter:
            state.aff.first.matter +
            state.aff.second.matter +
            state.aff.third.matter,
          method:
            state.aff.first.method +
            state.aff.second.method +
            state.aff.third.method,
          manner:
            state.aff.first.manner +
            state.aff.second.manner +
            state.aff.third.manner,
          total:
            state.aff.first.matter +
            state.aff.second.matter +
            state.aff.third.matter +
            state.aff.first.method +
            state.aff.second.method +
            state.aff.third.method +
            state.aff.first.manner +
            state.aff.second.manner +
            state.aff.third.manner,
        },
      },
      neg: {
        first: {
          matter: state.neg.first.matter,
          method: state.neg.first.method,
          manner: state.neg.first.manner,
          total:
            state.neg.first.matter +
            state.neg.first.method +
            state.neg.first.manner,
        },
        second: {
          matter: state.neg.second.matter,
          method: state.neg.second.method,
          manner: state.neg.second.manner,
          total:
            state.neg.second.matter +
            state.neg.second.method +
            state.neg.second.manner,
        },
        third: {
          matter: state.neg.third.matter,
          method: state.neg.third.method,
          manner: state.neg.third.manner,
          total:
            state.neg.third.matter +
            state.neg.third.method +
            state.neg.third.manner,
        },
        total: {
          matter:
            state.neg.first.matter +
            state.neg.second.matter +
            state.neg.third.matter,
          method:
            state.neg.first.method +
            state.neg.second.method +
            state.neg.third.method,
          manner:
            state.neg.first.manner +
            state.neg.second.manner +
            state.neg.third.manner,
          total:
            state.neg.first.matter +
            state.neg.second.matter +
            state.neg.third.matter +
            state.neg.first.method +
            state.neg.second.method +
            state.neg.third.method +
            state.neg.first.manner +
            state.neg.second.manner +
            state.neg.third.manner,
        },
      },
    }
  },

  delta: (state) => (speaker) => {
    return {
      matter: state[getTeam(speaker)][getSpeaker(speaker)].matter - 30,
      method: state[getTeam(speaker)][getSpeaker(speaker)].method - 15,
      manner: state[getTeam(speaker)][getSpeaker(speaker)].manner - 30,
    }
  },
}

export const mutations = {
  increment(state, payload) {
    /* Do a range check to ensure we won't increment out-of-bounds, noting that
     *  the range is different for different criteria (or "areas").
     */
    if (
      (payload.a === 'method' &&
        state[getTeam(payload.s)][getSpeaker(payload.s)][payload.a] >= 17) ||
      (payload.a !== 'method' &&
        state[getTeam(payload.s)][getSpeaker(payload.s)][payload.a] >= 34)
    ) {
      return
    }

    // Increment the criterion by one.
    state[getTeam(payload.s)][getSpeaker(payload.s)][payload.a] += 1

    // Return the mutated store.
    return state
    // Repeat as below for decrement mutation.
  },

  decrement(state, payload) {
    if (
      (payload.a === 'method' &&
        state[getTeam(payload.s)][getSpeaker(payload.s)][payload.a] <= 13) ||
      (payload.a !== 'method' &&
        state[getTeam(payload.s)][getSpeaker(payload.s)][payload.a] <= 26)
    ) {
      return
    }
    state[getTeam(payload.s)][getSpeaker(payload.s)][payload.a] -= 1
    return state
  },
}

function getSpeaker(number) {
  switch (number) {
    case 1:
      return 'first'
    case 2:
      return 'first'
    case 3:
      return 'second'
    case 4:
      return 'second'
    case 5:
      return 'third'
    case 6:
      return 'third'
  }
}

function getTeam(number) {
  if (number % 2 === 1) {
    return 'aff'
  }
  return 'neg'
}
