# M Cubed
M Cubed is a web-based calculator for adjudicators to calculate and verify scores for standard 3-on-3 debates.

The name "M-Cubed" comes from the 3 sub-criteria for assessing debates: matter, method and manner.

* [History](#history)
* [Built With](#built-with)
* [Build](#build)
* [Install](#install)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [License](#license)

## History
M-Cubed (originally the "Devious Adjudication Validator") was originally an [android app](https://gitlab.com/mraxel/M-Cubed-App) created as a calculator for calculating & verifying the scores of standard 3-on-3-style debates. App development is hard, and it only worked on Android. To make updates easier, and to make the calculator cross-platform it was re-written from scratch as a [website](https://gitlab.com/mraxel/M-Cubed-Live). Now we are re-writing from scratch to fix the many issues it has, and to hopefully one day make it an installable universal web app.

For the moment it is the [old web version](https://gitlab.com/mraxel/M-Cubed-Live) that is live on both [m3.axel.red](https://m3.axel.red) and [mmm.axel.red](https://mmm.axel.red/).

## Built With
* [Nuxt.js](https://nuxtjs.org/)

## Build

```bash
# install dependencies
$ yarn

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out the [Nuxt.js docs](https://nuxtjs.org).

## Install
There are a few different ways of running this:
- On a Node.js host
  1. Clone this repository to an environment with Node.js >= 14.0.0
  2. Run "yarn start".
- On a static host
  1. Clone this repository to an environment with Node.js >= 14.0.0
  2. Run "yarn generate".
  3. Copy the generated `dist/` folder to a static host
- As a Docker container
  1. Not currently supported (but planned for the future). For the moment you may try the instructions [available here](https://vuejs.org/v2/cookbook/dockerize-vuejs-app.html)
- Gitlab Pages
  1. Copy the `.gitlab-ci.yml` template from the [Gitlab pages template repository](https://gitlab.com/pages/nuxt).
  2. Replace every instance of `npm` with `yarn`.
  3. Change the artifact path from `public` to `dist`.
  4. Change the cache path from `node_modules/` to `.yarn/`.
  5. Commit this file to your GitLab repository and run the CI/CD pipeline.

## Usage
This is really easy: visit the URL where the website is hosted in a standard browser.

## Roadmap
* Support for Universal Web App
  * Installable on devices
  * Works offline
* Accessibility
  * Touchscreen support
  * Keyboard-only support
* Tracking/awarding of best speakers
* Sharing
  * Share a score-sheet by URL
  * Share a score-sheet by QR
* Customisation
  * Support for custom speaker names
  * Support for custom adjudicator names

## License
I can't really be bothered too much about licenses. As long as you don't cause me trouble, I won't cause you trouble. Therefore I am releasing under a MIT license. Please don't ask me about warranties and support and I won't send you copyright claims.

For more details see the [LICENSE](/LICENSE).

Some of the .svg icons in the `assets/svg` folder are from Google's [Material Design Icons library](https://material.io/resources/icons), freely available under a [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0.html) license.
